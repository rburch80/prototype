﻿using ReactiveUI;
using Splat;
using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype.WPF.ViewModels
{
    public class DataRequestorViewModel : ReactiveObject, IRoutableViewModel
    {
        public string UrlPathSegment { get; }
        public IScreen HostScreen { get; }

        public DataRequestorViewModel(IScreen screen = null)
        {
            HostScreen = screen ?? Locator.Current.GetService<IScreen>();
        }
    }
}