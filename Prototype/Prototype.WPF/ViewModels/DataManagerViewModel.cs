﻿using Dropbox.Api;
using Dropbox.Api.Files;
using Prototype.Domain.Services;
using Prototype.Domain.Services.Interfaces;
using ReactiveUI;
using Splat;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;

namespace Prototype.WPF.ViewModels
{
    public class DataManagerViewModel : ReactiveObject, IRoutableViewModel
    {
        public string UrlPathSegment { get; }
        public IScreen HostScreen { get; }
        public INetworkDriveService DbxService { get; }
        public ReactiveCommand<Unit, Unit> SaveCommandButton { get; }
        public DataManagerViewModel(IScreen screen = null, INetworkDriveService dbxService = null)
        {
            HostScreen = screen ??  Locator.Current.GetService<IScreen>();
            DbxService = dbxService ?? Locator.Current.GetService<INetworkDriveService>();

            SaveCommandButton = ReactiveCommand.CreateFromTask(async () => await SaveCommand());
        }

        private async Task SaveCommand()
        {
            using (var mem = new MemoryStream(0))
            {
                using var dbxClient = (DropboxClient)DbxService.Connect();
                var updated = await dbxClient.Files.UploadAsync(
                                                    "/test.txt",
                                                    WriteMode.Overwrite.Instance,
                                                    body: mem);
                Log.Debug("test.txt written to dropbox.");
            }
        }
    }
}
