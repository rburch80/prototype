﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Text;

namespace Prototype.WPF.ViewModels
{
    public class MainViewModel : ReactiveObject, IScreen
    {
        public RoutingState Router { get; }
        public ReactiveCommand<Unit, IRoutableViewModel> GoManager { get; }
        public ReactiveCommand<Unit, IRoutableViewModel> GoProvider { get; }
        public ReactiveCommand<Unit, IRoutableViewModel> GoRequestor { get; }
        public MainViewModel()
        {
            Router = new RoutingState();

            Router.NavigationStack.Add(new DataProviderViewModel());
            Router.NavigationStack.Add(new DataRequestorViewModel());
            Router.NavigationStack.Add(new DataManagerViewModel());
            

            GoManager = ReactiveCommand.CreateFromObservable(() => Router.Navigate.Execute(Router.FindViewModelInStack<DataManagerViewModel>()));

            GoProvider = ReactiveCommand.CreateFromObservable(() => Router.Navigate.Execute(Router.FindViewModelInStack<DataProviderViewModel>()));

            GoRequestor = ReactiveCommand.CreateFromObservable(() => Router.Navigate.Execute(Router.FindViewModelInStack<DataRequestorViewModel>()));
        }
    }
}
