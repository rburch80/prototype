﻿using Prototype.WPF.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prototype.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : ReactiveWindow<MainViewModel>
    {
        public MainWindow()
        {
            InitializeComponent();
            
            // This will load the global font styles so they are inherited by all controls
            Style = (Style)FindResource(typeof(Window));

            ViewModel = new MainViewModel();
            this.WhenActivated(disposables =>
            {
                this.OneWayBind(ViewModel, x => x.Router, x => x.RoutedViewHost.Router)
                    .DisposeWith(disposables);
                this.BindCommand(ViewModel, x => x.GoManager, x => x.GoManagerButton)
                    .DisposeWith(disposables);
                this.BindCommand(ViewModel, x => x.GoProvider, x => x.GoProviderButton)
                    .DisposeWith(disposables);
                this.BindCommand(ViewModel, x => x.GoRequestor, x => x.GoRequestorButton)
                    .DisposeWith(disposables);
            });
        }
    }
}
