﻿using Prototype.Domain.Services;
using Prototype.Domain.Services.Interfaces;
using Prototype.WPF.ViewModels;
using ReactiveUI;
using Serilog;
using Splat;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Windows;

namespace Prototype.WPF
{
    public class AppBootstrapper : IEnableLogger
    {
        public AppBootstrapper()
        {
            RegisterDependencies();
        }
        public void RegisterDependencies()
        {
            Locator.CurrentMutable.RegisterViewsForViewModels(Assembly.GetCallingAssembly());
            Locator.CurrentMutable.RegisterLazySingleton(() => new DropboxService(), typeof(INetworkDriveService));
            
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                //.WriteTo.Console()
                .WriteTo.File("log.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();
            
        }
    }
}
