﻿using Prototype.WPF.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Reactive.Disposables;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prototype.WPF.Views
{
    /// <summary>
    /// Interaction logic for DataManagerView.xaml
    /// </summary>
    public partial class DataManagerView : ReactiveUserControl<DataManagerViewModel>
    {
        public DataManagerView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.BindCommand(ViewModel, x => x.SaveCommandButton, x => x.SaveButton)
                .DisposeWith(disposables);
            });
        }
    }
}
