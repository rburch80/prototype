﻿using Prototype.WPF.ViewModels;
using ReactiveUI;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prototype.WPF.Views
{
  /// <summary>
  /// Interaction logic for DataProviderView.xaml
  /// </summary>
  public partial class DataProviderView : ReactiveUserControl<DataProviderViewModel>
  {
    public DataProviderView()
    {
      InitializeComponent();

            Log.Information("Hello, world!");

            int a = 10, b = 0;
            try
            {
                Log.Debug("Dividing {A} by {B}", a, b);
                Console.WriteLine(a / b);
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Something went wrong");
            }

        }
    }
}
