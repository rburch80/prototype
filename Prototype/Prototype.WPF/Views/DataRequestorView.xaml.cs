﻿using Prototype.WPF.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Prototype.WPF.Views
{
    /// <summary>
    /// Interaction logic for DataRequestorView.xaml
    /// </summary>
    public partial class DataRequestorView : ReactiveUserControl<DataRequestorViewModel>
    {
        public DataRequestorView()
        {
            InitializeComponent();
        }
    }
}
