﻿using Dropbox.Api;
using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype.Domain.Services.Interfaces
{
    public interface INetworkDriveService
    {
        object Connect();
        bool Save();
        bool Load();
    }
}
